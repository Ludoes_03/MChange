﻿using System;
using MChange.ViewModels;

namespace MChange.Infraestruture
{
    public class InstanceLocator
    {
        public MainViewModel Main { get; set; }

        public InstanceLocator()
        {
            Main = new MainViewModel();
        }
    }
}
